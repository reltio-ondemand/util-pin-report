package com.reltio.rocs;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;

import au.com.bytecode.opencsv.CSVWriter;

public class PinReport {

	static BufferedWriter output = null;
	static CSVWriter csvWriter = null;
	static String[] outputValues;
	static String link;
	static Properties properties = new Properties();
	
	
	private static final Logger LOGGER = LogManager.getLogger(PinReport.class.getName());
	private static final Logger LOGPERMANCE = LogManager.getLogger("performance-log");

	public static void main(String[] args) throws Exception {
		int noOfRecordsPerCall = 100;
		properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");

		Util.setHttpProxy(properties);
		
		final int MAX_QUEUE_SIZE_MULTIPLICATOR = 1;
		long processedCount = 0l;
		long programStartTime = System.currentTimeMillis();
		final String ENVIRONMENT_URL = properties.getProperty("ENVIRONMENT_URL");
		final String TENANT_ID = properties.getProperty("TENANT_ID");
		final String ENTITY_TYPE = properties.getProperty("ENTITY_TYPE");
		final String UI_PERSPECTIVE = properties.getProperty("UI_PERSPECTIVE");
		final String OUTPUT_FILE = properties.getProperty("OUTPUT_FILE");
		link = "https://"+ENVIRONMENT_URL + "ui/" + TENANT_ID + "/#p~" + UI_PERSPECTIVE + "_e~entities%2F";
		final int threads = Integer.parseInt(properties.getProperty("THREAD_COUNT","10"));
		final String timeLimit = properties.getProperty("TIME_LIMIT", "4000");
		
		Map<List<String>, List<String>> mutualExclusiveProps = new HashMap();

		mutualExclusiveProps.put(Arrays.asList("PASSWORD","USERNAME"), Arrays.asList("CLIENT_CREDENTIALS"));
		
		List<String> missingKeys = Util.listMissingProperties(properties,
                Arrays.asList("UI_PERSPECTIVE", "ENTITY_TYPE", "ENVIRONMENT_URL", "THREAD_COUNT", "TENANT_ID", "AUTH_URL","OUTPUT_FILE","TIME_LIMIT"));

        if (!missingKeys.isEmpty()) {
        	LOGGER.error("Following properties are missing from configuration file!! \n" + missingKeys);
            System.exit(0);
        }

		output = new BufferedWriter(new FileWriter(OUTPUT_FILE));
		csvWriter = new CSVWriter(output);
		outputValues = new String[5];
		outputValues[0] = "EntityType";
		outputValues[1] = "ReltioURI";
		outputValues[2] = "PinnedValue";
		outputValues[3] = "Attribute";
		outputValues[4] = "Link";
		csvWriter.writeNext(outputValues);
		output.flush();
		final String api ="https://"+ENVIRONMENT_URL + "/reltio/api/" + TENANT_ID + "/";

		 
		final ReltioAPIService reltioAPIService =Util.getReltioService(properties);
		
	
		String scan = reltioAPIService.post(api + "entities/_dbscan", "{\"type\":\"configuration/entityTypes/"+ ENTITY_TYPE + "\",\"pageSize\":\"200\",\"returnUriOnly\":true,\"timeLimit\":"+timeLimit+"}");
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode dbscan = objectMapper.readValue(scan, JsonNode.class);
		//ExecutorService executorService = Executors.newFixedThreadPool(threads);
		ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(threads);

		List<Future<Long>> futures = new ArrayList<Future<Long>>();
		
		int temp = 0;
		while (dbscan.get("uris") != null) {
			LOGGER.info("Iteration : " + temp);
			
			for (final JsonNode ctr : dbscan.get("uris")) {
				
				futures.add(executorService.submit(new Callable<Long>() {
					public Long call() throws Exception {
						String urii = ctr.toString().replace("\"", "");
						extract(link, api, urii, reltioAPIService);
						
						return System.currentTimeMillis();
					}}));
			}
			//waitForTasksReady(futures, threads * 2);	
			
			processedCount+=waitForTasksReady(futures, threads * (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
			if (processedCount > 0) {
				printPerformanceLog(executorService.getCompletedTaskCount() * noOfRecordsPerCall,
						processedCount, programStartTime, threads);
			}
			dbscan = scan(dbscan.get("cursor"), reltioAPIService, api);
			temp++;
		}
		waitForTasksReady(futures, 0);
		Util.close(output,csvWriter);
	}
	
	public static long waitForTasksReady(Collection<Future<Long>> futures, int maxNumberInList) {
		long totalResult = 0l;
		while (futures.size() > maxNumberInList) {
			try {
				Thread.sleep(20);
			} catch (Exception e) {
				// ignore it...
			}
			for (Future<Long> future : new ArrayList<Future<Long>>(futures)) {
				if (future.isDone()) {
					try {
						totalResult += future.get();
						futures.remove(future);
					} catch (Exception e) {
						LOGGER.error(e.getMessage());
						LOGGER.debug(e);
					}
				}
			}
		}
		return totalResult;
	}


	static JsonNode scan(JsonNode cursor, ReltioAPIService reltioAPIService, String api) throws GenericException,
			ReltioAPICallFailureException, JsonParseException, JsonMappingException, IOException {
		String scanObj = reltioAPIService.post(api + "entities/_dbscan", cursor.toString());
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode dbscanObj = objectMapper.readValue(scanObj, JsonNode.class);
		return dbscanObj;
	}

	static void extract(String link, String api, String uri, 
			ReltioAPIService reltioAPIService) throws Exception {

		/*
		 * An extract into Excel that lists each entity where a pin has been set for any
		 * of the attributes. The row lists the entityID, full url of the entity (that
		 * user can click on), entity.Name, attribute that conains a pin, pinned value,
		 * Date pinned, length of time since pinned (e.g. 237 days)
		 */
		outputValues[0] = "";
		outputValues[1] = "";
		outputValues[2] = "";
		outputValues[3] = "";
		outputValues[4] = "";
		String getEntity = api + uri;
		String response = reltioAPIService.get(getEntity);
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode json = objectMapper.readValue(response, JsonNode.class);
		JsonNode attributes = json.get("attributes");

		outputValues[0] = json.get("type").toString().replace("\"", "").substring(26);
		outputValues[1] = json.get("uri").toString().replace("\"", "").substring(9);
		outputValues[4] = link + outputValues[1];

		for (JsonNode attr : attributes) {

			for (JsonNode inAttr : attr) {

				if (inAttr.has("pin") && inAttr.has("type")) {
					outputValues[2] = inAttr.get("value").toString().replace("\"", "");
					String Values[] = inAttr.get("uri").toString().split("/");
					outputValues[3] = Values[3];
					csvWriter.writeNext(outputValues);
					output.flush();
				} else if (inAttr.has("pin") && !inAttr.has("type")) {
					outputValues[2] = inAttr.get("label").toString().replace("\"", "");
					String Values[] = inAttr.get("uri").toString().split("/");
					outputValues[3] = Values[3];
					csvWriter.writeNext(outputValues);
					output.flush();
					JsonNode nestedAttr = inAttr.get("value");
					for (JsonNode val : nestedAttr) {
						if (val.get(0).has("pin")) {
							outputValues[2] = val.get(0).get("value").toString().replace("\"", "");
							String Value[] = val.get(0).get("uri").toString().split("/");
							outputValues[3] = Value[3] + "/" + Value[5];
							csvWriter.writeNext(outputValues);
							output.flush();
						}
					}
				} else if (!inAttr.has("type")) {
					JsonNode nestedAttr = inAttr.get("value");
					for (JsonNode val : nestedAttr) {
						if (val.get(0).has("pin")) {
							outputValues[2] = val.get(0).get("value").toString().replace("\"", "");
							String Value[] = val.get(0).get("uri").toString().split("/");
							outputValues[3] = Value[3] + "/" + Value[5];
							csvWriter.writeNext(outputValues);
							output.flush();
						}
					}
				}

			}
		}
	}

	public static void printPerformanceLog(long totalTasksExecuted, long totalTasksExecutionTime, long programStartTime,
			long numberOfThreads) {
		LOGGER.info("[Performance]: ============= Current performance status (" + new Date().toString()
				+ ") =============");
		long finalTime = System.currentTimeMillis() - programStartTime;
		LOGGER.info("[Performance]:  Total processing time : " + finalTime);

		LOGGER.info("[Performance]:  total task executed : " + totalTasksExecuted);
		LOGGER.info("[Performance]:  Total OPS (Match Pairs extracted for Entities / Time spent from program start): "
				+ (totalTasksExecuted / (finalTime / 1000f)));
		LOGGER.info(
				"[Performance]:  Total OPS without waiting for queue (Match Pairs extracted for Entities / (Time spent from program start - Time spent in waiting for API queue)): "
						+ (totalTasksExecuted / ((finalTime) / 1000f)));
		LOGGER.info(
				"[Performance]:  API Server data load requests OPS (Match Pairs extracted for Entities / (Sum of time spend by API requests / Threads count)): "
						+ (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
		LOGGER.info(
				"[Performance]: ===============================================================================================================");

		// log performance only in separate logs
		LOGPERMANCE.info("[Performance]: ============= Current performance status (" + new Date().toString()
				+ ") =============");
		LOGPERMANCE.info("[Performance]:  Total processing time : " + finalTime);

		LOGPERMANCE.info("[Performance]:  Entities sent: " + totalTasksExecuted);
		LOGPERMANCE
				.info("[Performance]:  Total OPS (Match Pairs extracted for Entities / Time spent from program start): "
						+ (totalTasksExecuted / (finalTime / 1000f)));
		LOGPERMANCE
				.info("[Performance]:  Total OPS without waiting for queue (Match Pairs extracted for Entities / (Time spent from program start - Time spent in waiting for API queue)): "
						+ (totalTasksExecuted / ((finalTime) / 1000f)));
		LOGPERMANCE
				.info("[Performance]:  API Server data load requests OPS (Match Pairs extracted for Entities / (Sum of time spend by API requests / Threads count)): "
						+ (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
		LOGPERMANCE.info(
				"[Performance]: ===============================================================================================================");
	}
}
