
# PIN REPORT FOR entities

## Description
This utility will generate report for the pinned attributes for specific entities.


##Change Log

```
#!plaintext

Last Update Date: 30/09/2019
Version: 0.0.9
Description:Updated the utility version to three digit,JAR name standardization.
CLIENT_CREDENTIALS = This property used for to get the access token using client_credentials grant type. The value for this property can be obtained by encoding the client name and secret separated by colon in Base64 format. (clientname:clientsecret)

Last Update Date: 06/25/2019
Version: 0.0.8
Description:
Utility cleanup, removed older jars,package standardization.
And proxy change

Last Update Date: 03/14/2019
Version: 0.0.7
Description: 
1) Clear validation message when properties are missing
2) Encryption of password in properties file
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```


