# Quick Start 

##Building
The main method of the application is at the following   
path: com.reltio.rocs.PinReport

##Dependencies 


1) reltio-cst-core 1.4.9

##Parameters File Example

```
#Common Properties
ENVIRONMENT_URL = https://pilot.reltio.com
TENANT_ID = 0gADripE92rvwia
AUTH_URL = https://auth.reltio.com/oauth/token
USERNAME = srikant.prabhat@reltio.com
PASSWORD = *********
CLIENT_CREDENTIALS=
ENTITY_TYPE = Organization
THREAD_COUNT=25
HTTP_PROXY_HOST=
HTTP_PROXY_PORT=


#Tool Specific Properties
UI_PERSPECTIVE = com.reltio.plugins.entity.org.HCOPerspective
OUTPUT_FILE = pin-report123.csv
```
##Executing

Command to start the utility.
```
#!plaintext

#Windows
java   -jar   util-pin-report-{{version}}-jar-with-dependencies.jar “configuration.properties” > $logfilepath$'

#Unix
java   -jar   reltio-util-pinreport-{{version}}-jar-with-dependencies.jar “configuration.properties” > $logfilepath$'

Please take the latest version from the Bitbucket.
```